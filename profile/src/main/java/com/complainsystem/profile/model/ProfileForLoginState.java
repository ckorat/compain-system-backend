package com.complainsystem.profile.model;

public class ProfileForLoginState {
	private int id;
	private String username;
	private String role;

	public ProfileForLoginState() {
		super();
	}

	public ProfileForLoginState(int id, String username, String role) {
		super();
		this.id = id;
		this.username = username;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
