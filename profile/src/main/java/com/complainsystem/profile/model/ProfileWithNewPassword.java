package com.complainsystem.profile.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ProfileWithNewPassword {
	@Min(1)
	private int id;
	@NotNull
	private String oldPassword;
	@NotNull
	private String newPassword;

	public ProfileWithNewPassword() {
		super();
	}

	public ProfileWithNewPassword(int id, String oldPassword, String newPassword) {
		super();
		this.id = id;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return "ProfileWithNewPassword [id=" + id + ", oldPassword=" + oldPassword + ", newPassword=" + newPassword
				+ "]";
	}

}
