package com.complainsystem.profile.model;

import java.sql.Date;

import javax.validation.constraints.NotNull;

public class ProfileWithPassword extends Profile {
	@NotNull
	String password;

	public ProfileWithPassword() {
		super();
	}

	public ProfileWithPassword(int profileId, String username, String firstname, String lastname, String email,
			String contact, Date dob, String address, String city, String state, String pincode, boolean status,
			String role) {
		super(profileId, username, firstname, lastname, email, contact, dob, address, city, state, pincode, status,
				role);

	}

	public ProfileWithPassword(String password) {
		super();
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileWithPassword other = (ProfileWithPassword) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProfileWithPassword [password=" + password + "," + super.toString() + "]";
	}

}
