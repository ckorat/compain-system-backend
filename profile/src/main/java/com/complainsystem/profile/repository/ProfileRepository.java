package com.complainsystem.profile.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.complainsystem.profile.model.Profile;
import com.complainsystem.profile.model.ProfileAtLogin;
import com.complainsystem.profile.model.ProfileForLoginState;
import com.complainsystem.profile.model.ProfileWithNewPassword;
import com.complainsystem.profile.model.ProfileWithPassword;

@Repository
public interface ProfileRepository {
	public List<Profile> findAllProfiles();

	public List<Profile> findProfileById(int id) throws Exception;

	public List<Profile> findAllProfilesByRole(String role) throws Exception;

	public int addProfile(ProfileWithPassword profile) throws Exception;

	public boolean updateProfile(int id, Profile profile) throws Exception;

	public boolean updateStatus(int id, boolean status) throws Exception;

	public int updatePassword(ProfileWithNewPassword profileWithNewPassword) throws Exception;

	public ProfileForLoginState authenticate(ProfileAtLogin profileAtLogin) throws Exception;

}
