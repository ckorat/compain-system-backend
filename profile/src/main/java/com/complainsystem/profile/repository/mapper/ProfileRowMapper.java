package com.complainsystem.profile.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.complainsystem.profile.model.Profile;

public class ProfileRowMapper implements RowMapper<Profile> {

	@Override
	public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
		Profile profile = new Profile();
		profile.setProfileId(rs.getInt("id"));
		profile.setUsername(rs.getString("username"));
		profile.setFirstname(rs.getString("firstname"));
		profile.setLastname(rs.getString("lastname"));
		profile.setEmail(rs.getString("email"));
		profile.setContact(rs.getString("contact"));
		profile.setDob(rs.getDate("dob"));
		profile.setAddress(rs.getString("address"));
		profile.setCity(rs.getString("city"));
		profile.setState(rs.getString("state"));
		profile.setPincode(rs.getString("pincode"));
		profile.setStatus(rs.getBoolean("status"));
		profile.setRole(rs.getString("role"));
		return profile;
	}

}
