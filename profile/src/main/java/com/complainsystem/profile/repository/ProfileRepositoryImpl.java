package com.complainsystem.profile.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.complainsystem.profile.model.Profile;
import com.complainsystem.profile.model.ProfileAtLogin;
import com.complainsystem.profile.model.ProfileForLoginState;
import com.complainsystem.profile.model.ProfileWithNewPassword;
import com.complainsystem.profile.model.ProfileWithPassword;
import com.complainsystem.profile.repository.mapper.ProfileRowMapper;
import com.complainsystem.queryhelper.QueryGenerator;

@Repository(value = "profileRepo")
public class ProfileRepositoryImpl implements ProfileRepository {

	static String fields = "`id`, `username`, `firstname`, `lastname`, `email`, `contact`, `dob`, `address`,"
			+ " `city`, `state`, `pincode`, `status`, `role`";
	static String tableName = "profiles";
	private QueryGenerator<ProfileWithPassword> queryGenerator = new QueryGenerator<ProfileWithPassword>();

	@Autowired

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Override
	public List<Profile> findAllProfiles() {
		String sql = "select " + fields + " from " + tableName;
		return jdbcTemplate.query(sql, new ProfileRowMapper());
	}

	@Override
	public List<Profile> findProfileById(int id) throws Exception {
		String sql = "select " + fields + " from " + tableName + " where id=" + id;
		return jdbcTemplate.query(sql, new ProfileRowMapper());
	}

	@Override
	public List<Profile> findAllProfilesByRole(String role) throws Exception {
		String sql = "select " + fields + " from " + tableName + " where role='" + role + "'";
		return jdbcTemplate.query(sql, new ProfileRowMapper());
	}

	@Override
	public int addProfile(ProfileWithPassword profileWithPassword) throws Exception {
		KeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(queryGenerator.generatePreparedStatementInsertQuery(tableName, profileWithPassword),
				new BeanPropertySqlParameterSource(profileWithPassword), holder);
		return holder.getKey().intValue();
	}

	@Override
	public boolean updateProfile(int id, Profile profile) throws Exception {
		String sql = "UPDATE `" + tableName + "` set "
				+ "`username` = :username, `firstname` = :firstname, `lastname`= :lastname, `contact`= :contact, `dob`=:dob, "
				+ " `city`=:city,`address`=:address, `state`=:state, `pincode`=:pincode, `status`=:status, `role`=:role where `id`=:profileId";
		profile.setProfileId(id);
		return jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(profile)) > 0;
	}

	@Override
	public boolean updateStatus(int id, boolean status) throws Exception {
		String sql = "UPDATE `" + tableName + "` set `status`=:status where `id`=:profileId";
		Profile profile = new Profile();
		profile.setProfileId(id);
		profile.setStatus(status);
		return jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(profile)) > 0;
	}

	@Override
	public int updatePassword(ProfileWithNewPassword profileWithNewPassword) throws Exception {
		String selectSQL = "select id from " + tableName + " where id=:id and password=:oldPassword";
		if (jdbcTemplate.queryForList(selectSQL, new BeanPropertySqlParameterSource(profileWithNewPassword))
				.size() > 0) {
			String sql = "UPDATE `" + tableName + "` set `password`=:newPassword  where `id`=:id";
			return jdbcTemplate.update(sql, new BeanPropertySqlParameterSource(profileWithNewPassword));
		}
		return -1;
	}

	@Override
	public ProfileForLoginState authenticate(ProfileAtLogin profileAtLogin) throws Exception {
		String sql = "select `id`,`username`,`role`,`status` from `" + tableName + "` where `username`=:username and "
				+ "`password`=:password";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql,
				new BeanPropertySqlParameterSource(profileAtLogin));
		if (list.size() > 0) {
			if (!(boolean) list.get(0).get("status")) {
				return new ProfileForLoginState();
			} else {
				return new ProfileForLoginState(Integer.parseInt(list.get(0).get("id") + ""),
						list.get(0).get("username") + "", list.get(0).get("role") + "");
			}
		}
		return null;
	}
}
