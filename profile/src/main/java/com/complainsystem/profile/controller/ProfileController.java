package com.complainsystem.profile.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.complainsystem.profile.model.Profile;
import com.complainsystem.profile.model.ProfileAtLogin;
import com.complainsystem.profile.model.ProfileForLoginState;
import com.complainsystem.profile.model.ProfileWithNewPassword;
import com.complainsystem.profile.model.ProfileWithPassword;
import com.complainsystem.profile.service.ProfileService;
import com.complainsystem.response.Result;

@RestController()
@RequestMapping(path = "/profiles", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProfileController {

	@Autowired
	ProfileService profileService;

	@GetMapping("/")
	public ResponseEntity<Result<List<Profile>>> getAllProfile() {
		Result<List<Profile>> result = profileService.findAllProfiles();
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Result<Profile>> getProfileById(@PathVariable("id") @Valid @Pattern(regexp = "[0-9]*") int id)
			throws Exception {
		Result<Profile> result = profileService.findProfilesById(id);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@GetMapping("/role/{role}")
	public ResponseEntity<Result<List<Profile>>> getAllProfilesByRole(
			@PathVariable("role") @Valid @Pattern(regexp = "(user|employee|admin)") String role) throws Exception {
		Result<List<Profile>> result = profileService.findAllProfilesByRole(role);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@PostMapping("/")
	public ResponseEntity<Result<Profile>> addProfile(
			@RequestBody(required = true) @Valid ProfileWithPassword profileWithPassword) throws Exception {
		Result<Profile> result = profileService.addProfile(profileWithPassword);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Result<Profile>> updateProfile(@PathVariable("id") @Valid @Pattern(regexp = "[0-9]*") int id,
			@RequestBody(required = true) @Valid Profile profile) throws Exception {
		Result<Profile> result = profileService.updateProfile(id, profile);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@PutMapping("/{id}/{status}")
	public ResponseEntity<Result<String>> updateStatus(@PathVariable("id") @Valid @Pattern(regexp = "[0-9]*") int id,
			@PathVariable("status") @Valid @Pattern(regexp = "(true|false)") boolean status) throws Exception {
		Result<String> result = profileService.updateStatus(id, status);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@PutMapping("/")
	public ResponseEntity<Result<String>> updatePassword(
			@RequestBody(required = true) @Valid ProfileWithNewPassword profileWithNewPassword) throws Exception {
		Result<String> result = profileService.updatePassword(profileWithNewPassword);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}

	@PostMapping("/authenticate")
	public ResponseEntity<Result<ProfileForLoginState>> authenticate(
			@RequestBody(required = true) @Valid ProfileAtLogin profileAtLogin) throws Exception {
		Result<ProfileForLoginState> result = profileService.authenticate(profileAtLogin);
		return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
	}
}
