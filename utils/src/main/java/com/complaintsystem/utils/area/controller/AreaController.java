package com.complaintsystem.utils.area.controller;

import com.complainsystem.response.Result;
import com.complaintsystem.utils.area.service.AreaService;
import com.complaintsystem.utils.area.model.Area;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/areas",produces = "application/json")
@ApiResponses(value =
        {
                @ApiResponse(code = 400, message = "Bad Request", response = Exception.class),
                @ApiResponse(code = 403, message = "Forbidden", response = Exception.class),
                @ApiResponse(code = 404, message = "Not Found", response = Exception.class),
                @ApiResponse(code = 500, message = "Technical Error", response = Exception.class)
        })
public class AreaController {

    @Autowired
    AreaService areaService;

    @ApiOperation(value = "list", notes = "Returns the list of Areas")
    @ApiResponses(value =
            {
                    @ApiResponse(code = 200, message = "Successful", response = List.class)
            })

    @GetMapping(value = "/",produces = "application/json")
    public ResponseEntity<Result<List<Area>>> getAllArea() throws Exception {
        Result<List<Area>> result = areaService.getAllAreas();
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @ApiOperation(value = "list", notes = "Returns the list of Active Areas")
    @ApiResponses(value =
            {
                    @ApiResponse(code = 200, message = "Successful", response = List.class)
            })
    @GetMapping(value = "/active/",produces = "application/json")
    public ResponseEntity<Result<List<Area>>> getActiveArea() throws Exception {
        Result<List<Area>> result=areaService.getActiveAreas();
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @ApiOperation(value = "Get an area by Id")
    @ApiResponses(value =
            {
                    @ApiResponse(code = 200, message = "Successful", response = Area.class)
            })
    @GetMapping(value = "/{id}",produces = "application/json")
    public ResponseEntity<Result<Area>> getAreaById(@ApiParam(value = "Area id from which Area object will retrieve", required = true) @PathVariable(value = "id") String id) throws Exception {
        Result<Area> result=areaService.getAreaById(id);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @ApiOperation(value = "Add an Area")
    @ApiResponses(value =
            {
                    @ApiResponse(code = 201, message = "Successful created")
            })
    @PostMapping(value = "/",consumes = "application/json")
    public ResponseEntity<Result<Area>> addArea(@ApiParam(value = "Area object store in database table", required = true) @RequestBody @Valid Area area) throws Exception {
        Result<Area> result  =areaService.addArea(area);
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }

    @ApiOperation(value = "Update an Area")
    @ApiResponses(value =
            {
                    @ApiResponse(code = 200, message = "Successful")
            })
    @PutMapping(value = "/",consumes = "application/json")
    public ResponseEntity<Result<Area>> updateArea(@ApiParam(value = "Update Area object", required = true)  @RequestBody @Valid Area area) throws Exception {
        Result<Area> result = new Result(200, "Updated Successfully");
        if (areaService.updateArea(area.getAreaId(), area)) {
            result.setData(area);
        }
        return new ResponseEntity<>(result,HttpStatus.valueOf(result.getCode()));
    }
}

