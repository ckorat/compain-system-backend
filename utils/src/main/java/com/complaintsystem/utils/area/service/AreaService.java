package com.complaintsystem.utils.area.service;

import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import com.complaintsystem.utils.area.model.Area;
import com.complaintsystem.utils.area.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class AreaService {

    @Autowired
    @Qualifier("areaRepository")
    AreaRepository areaRepository;

    public Result<List<Area>> getAllAreas() throws Exception {
        List<Area> list = areaRepository.getAllAreas();
        if(!list.isEmpty()){
            return new Result<>(200, list);
        }
        throw new ResultException(new Result<>(500,"no data found"));
    }
    public Result<List<Area>> getActiveAreas() throws Exception {
        List<Area> list = areaRepository.getActiveArea();
        if(!list.isEmpty()){
            return new Result<>(200,list);
        }
        throw new ResultException(new Result<>(500,"no Active area found"));
    }

    public Result<Area> getAreaById(String id) throws Exception {
        List<Area> list = areaRepository.getAreaById(id);
        if (list.size() > 0) {
            return new Result<>(200,list.get(0)) ;
        }
        throw  new ResultException(new Result<>(500,"no data found" + id));
    }

    public Result<Area> addArea(Area area) throws Exception {
        int result = areaRepository.addArea(area);
        Result<Area> resultObj = new Result<>();
        if (result == 0) {
            return new Result<>(201,"Succesfully added!",area);
        }else if(result == 1){
            throw new ResultException(new Result<>(400, "Area name should be unique!"));
        }else if(result==2){
            throw new ResultException(new Result<>(400, "pincode should be unique!"));
        }else{
            throw new ResultException(new Result<>(400,"error",
                    new ArrayList<>(Arrays.asList(
                            new Result.ComplainSystemError("error".hashCode(),"Area name should be unique!"),
                            new Result.ComplainSystemError("error".hashCode(),"pincode should be unique!")
                    ))));
        }

    }

    public boolean updateArea(int id, Area area) throws Exception {
        if(areaRepository.updateArea(id,area)){
            return true;
        }
        throw new ResultException(new Result<>(500,"unable to update the given data"));
    }

}
