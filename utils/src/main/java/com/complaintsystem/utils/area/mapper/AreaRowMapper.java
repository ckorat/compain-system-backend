package com.complaintsystem.utils.area.mapper;

import com.complaintsystem.utils.area.model.Area;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AreaRowMapper implements RowMapper<Area> {
    @Override
    public Area mapRow(ResultSet rs, int rowNum) throws SQLException {
        Area area = new Area();
        area.setAreaId(rs.getInt("areaid"));
        area.setName(rs.getString("name"));
        area.setPincode(rs.getInt("pincode"));
        area.setDescription(rs.getString("description"));
        area.setStatus(rs.getInt("status"));
        return area;
    }
}