package com.complaintsystem.utils.area.repository;

import com.complaintsystem.utils.area.mapper.AreaRowMapper;
import com.complaintsystem.utils.area.model.Area;
import com.complaintsystem.utils.area.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "areaRepository")
public class AreaRepositoryImpl implements AreaRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    QueryService queryService;

    @Override
    public List<Area> getAllAreas() {
        return jdbcTemplate.query(queryService.getDbQueries().getGetAllArea(),new AreaRowMapper());
    }

    @Override
    public List<Area> getAreaById(String id) {
        return jdbcTemplate.query(queryService.getDbQueries().getGetAreaById(),new AreaRowMapper(),id);
    }

    @Override
    public int addArea(Area area) {
        int a =0;
        if(jdbcTemplate.query(queryService.getDbQueries().getIsAreaNameUnique(),new AreaRowMapper(),area.getName()).size()>0){
            a=a+ 1;
        }
        if(jdbcTemplate.query(queryService.getDbQueries().getIsAreaPincodeUnique(),new AreaRowMapper(),area.getPincode()).size()>0){
            a=a+ 2;
        }
        if(a==0) {
            if(jdbcTemplate.update(queryService.getDbQueries().getInsertArea(), area.getName(), area.getPincode(), area.getDescription(), area.getStatus())>0){
                    a=0;
            } else{
                a=-1;
            }
        }
        return a;
    }

    @Override
    public boolean updateArea(int id, Area area) {
        return jdbcTemplate.update(queryService.getDbQueries().getUpdateArea(), area.getName(), area.getPincode(), area.getDescription(), area.getStatus(),id) > 0;
    }

    @Override
    public List<Area> getActiveArea() {
        return jdbcTemplate.query(queryService.getDbQueries().getGetActiveArea(),new AreaRowMapper());
    }
}

