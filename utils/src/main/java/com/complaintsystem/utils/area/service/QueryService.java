package com.complaintsystem.utils.area.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "db")
@Component
public class QueryService {

    @NotNull
    public DbQueries dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries{
        private String insertArea;
        private String updateArea;
        private String getAllArea;
        private String getAreaById;
        private String getActiveArea;
        private String isAreaNameUnique;
        private String isAreaPincodeUnique;
    }
}
