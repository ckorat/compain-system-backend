package com.complaintsystem.utils.area.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "All details about the Area. ")
public class Area {
    @ApiModelProperty(notes = "The database generated Area ID")
    private int areaId;
    @ApiModelProperty(notes = "The area name")
    @NotNull
    private String name;
    @ApiModelProperty(notes = "The area pincode")
    private int pincode;
    @ApiModelProperty(notes = "The area discription")
    @NotNull
    private String description;
    @ApiModelProperty(notes = "The area status")
    private int status;
}
