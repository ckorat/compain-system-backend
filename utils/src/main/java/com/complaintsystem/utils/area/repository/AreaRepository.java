package com.complaintsystem.utils.area.repository;
import com.complaintsystem.utils.area.model.Area;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AreaRepository {
    List<Area> getAllAreas() throws Exception;
    List<Area> getAreaById(String id) throws Exception;
    int addArea(Area area) throws Exception;
    boolean updateArea(int id, Area area) throws Exception;
    List<Area> getActiveArea() throws Exception;
}

