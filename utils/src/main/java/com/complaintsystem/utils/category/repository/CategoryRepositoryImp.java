package com.complaintsystem.utils.category.repository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.complaintsystem.utils.category.model.Category;
import com.complaintsystem.utils.category.model.mapper.CategoryRowMapper;
import com.complaintsystem.utils.category.properties.CategoryProperties;
import java.util.List;


@Repository(value = "CategoryRepo")
public class CategoryRepositoryImp implements CategoryRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    CategoryProperties categoryProperties;

    @Override
    public List<Category> findAllCategories()throws  Exception{
    	return jdbcTemplate.query(categoryProperties.getDbQueries().getGetCategories(), new CategoryRowMapper());
     
    }


    @Override
    public List<Category> findCategoryById(String id) throws Exception {
        return  jdbcTemplate.query(categoryProperties.getDbQueries().getGetCategoryById(), new CategoryRowMapper(), id);

    }

    @Override
    public Integer addCategory(Category category) throws Exception {
        return jdbcTemplate.update(categoryProperties.getDbQueries().getInsertCategory(), category.getCategoryName(), category.getDescription(), category.isStatus());

    }

    @Override
    public Integer updateCategory(int id, Category category) throws Exception {
        return jdbcTemplate.update(categoryProperties.getDbQueries().getUpdateCategory(), category.getCategoryName(), category.getDescription(), category.isStatus(), id);

    }


    public List<Category> findAllActiveCategories(boolean status) throws Exception {
        return  jdbcTemplate.query(categoryProperties.getDbQueries().getFindAllActiveCategories(), new CategoryRowMapper(), status);

    }


}





