package com.complaintsystem.utils.category.service;

import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import com.complaintsystem.utils.category.model.Category;
import com.complaintsystem.utils.category.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public Result<List<Category>> findAllCategories() throws Exception {
        List<Category> list = categoryRepository.findAllCategories();
        Result result = null;
        if (list != null) {
            result = new Result<>(200, list);
        } else {
			throw new ResultException(new Result<>(404, "no Categories's found, please try again!",
					new ArrayList<>(Arrays.asList(new Result.ComplainSystemError(("").hashCode(),
							"Categories does not exists")))));
        }
        return result;
    }

    public Result<Category> findCategoryById(String id) throws Exception {
        List<Category> list = categoryRepository.findCategoryById(id);
        if (!list.isEmpty())
            return new Result<>(200, list.get(0));
        else
			throw new ResultException(new Result<>(404, "no Categories's found, please try again!",
					new ArrayList<>(Arrays.asList(new Result.ComplainSystemError((id + "").hashCode(),
							"Category with given id('" + id + "') does not exists")))));
    }

    public Result<Integer> addCategory(Category category) throws Exception {
     int success= categoryRepository.addCategory(category);
        if(success>0) {
            return new Result<>(200,"Records added successfully");
        }
        else {
            throw new ResultException(new Result<>(400, "please try again!",
                    new ArrayList<>(Arrays.asList(new Result.ComplainSystemError(("").hashCode(),
                            "Please try again")))));
        }

    }

    public Result<Integer> updateCategory(int id, Category category) throws Exception {
        int success=categoryRepository.updateCategory(id, category);
        if(success>0) {
            return new Result<>(200,"Records updated successfully");

        }
        else {
            throw new ResultException(new Result<>(400, "please try again!",
                    new ArrayList<>(Arrays.asList(new Result.ComplainSystemError(("").hashCode(),
                            "Please try again")))));
        }
    }

    public Result<List<Category>> findAllActiveCategories(boolean status) throws Exception {
        List<Category> list = categoryRepository.findAllActiveCategories(status);
        if (!list.isEmpty())
            return new Result<>(200, list);
        else
            throw new ResultException(new Result<>(404, "no Categories's found, please try again!",
                    new ArrayList<>(Arrays.asList(new Result.ComplainSystemError((status + "").hashCode(),
                            "Category with given status('" + status + "') does not exists")))));
    }


}
