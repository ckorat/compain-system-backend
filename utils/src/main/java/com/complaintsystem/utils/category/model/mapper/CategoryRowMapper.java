package com.complaintsystem.utils.category.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.complaintsystem.utils.category.model.*;

public class CategoryRowMapper implements RowMapper<Category> {

	@Override
	public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
		Category category = new Category();
		category.setCategoryId(rs.getInt("Catid"));
		category.setCategoryName(rs.getString("catname"));
		category.setDescription(rs.getString("description"));
		category.setStatus(rs.getBoolean("status"));
		return category;
	}

}
