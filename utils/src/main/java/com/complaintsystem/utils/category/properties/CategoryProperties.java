package com.complaintsystem.utils.category.properties;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ToString
@Builder
@ConfigurationProperties(prefix = "db")
public class CategoryProperties {

    @NotNull
    public DbQueries dbQueries;
    
    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries{
        private String insertCategory;
        private String getCategories;
        private String getCategoryById;
        private String updateCategory;
        private String findAllActiveCategories;
    }
}

