package com.complaintsystem.utils.category.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.complainsystem.response.Result;
import com.complaintsystem.utils.category.exception.CategoryException;
import com.complaintsystem.utils.category.model.Category;
import com.complaintsystem.utils.category.service.CategoryService;

import java.util.List;

@RestController()
@RequestMapping(value = "/categories", produces = "application/json")
@ApiResponses(value =
        {
                @ApiResponse(code = 400, message = "Bad Request", response = CategoryException.class),
                @ApiResponse(code = 403, message = "Forbidden", response = CategoryException.class),
                @ApiResponse(code = 404, message = "Not Found", response = CategoryException.class),
                @ApiResponse(code = 500, message = "Technical Error", response = CategoryException.class)
        })
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @ApiOperation(value = "list", notes = "Returns the list of category")
    @GetMapping("/")
    public ResponseEntity<Result<List<Category>>> getAllCategories() throws Exception {
        Result<List<Category>> result = categoryService.findAllCategories();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Result<Category>> getCategoryById(@PathVariable("id") String id) throws Exception {
        Result<Category> result = categoryService.findCategoryById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Result<Integer>> addCategory(@RequestBody Category category) throws Exception {
        Result<Integer> result = categoryService.addCategory(category);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @PutMapping("/{id}")
    public ResponseEntity<Result<Integer>> updateCategory(@PathVariable("id") int id, @RequestBody Category category) throws Exception {
        Result<Integer> result = categoryService.updateCategory(id, category);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/status/{status}")
    public ResponseEntity<Result<List<Category>>> findAllActiveCategories(@PathVariable("status") boolean status) throws Exception {
        Result<List<Category>> result = categoryService.findAllActiveCategories(status);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }


}
