package com.complaintsystem.utils.category.repository;
import java.util.List;
import com.complaintsystem.utils.category.model.Category;

public interface CategoryRepository {
	
	public List<Category> findAllCategories() throws Exception;
	
    public List<Category> findCategoryById(String id) throws Exception;

	public Integer addCategory(Category category) throws Exception;

	public Integer updateCategory(int id, Category category) throws Exception;

	public List<Category> findAllActiveCategories(boolean status) throws Exception;


}
