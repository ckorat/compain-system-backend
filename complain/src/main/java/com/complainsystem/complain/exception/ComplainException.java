package com.complainsystem.complain.exception;

import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.*;

@RestControllerAdvice
public class ComplainException extends ResponseEntityExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ComplainException.class);

    @ExceptionHandler(Exception.class)
    private ResponseEntity<Map<String, Object>> Exception(Exception ex) {
        Map<String, Object> map = new HashMap<>();
        map.put("error id", ex.getMessage().hashCode());
        map.put("message", "unfortunately! there was some error at the server side.");
        logger.error("\n" + ex.getMessage().hashCode() + "\n" + ex.getMessage());
        return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ResultException.class})
    private ResponseEntity<Result<Object>> resultException(ResultException ex) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        Result<Object> result = ex.getResultExecption();
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
                                                        HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        return new ResponseEntity<>(
                new Result<>(400, "Error! unable to parse the given data",
                        new ArrayList<>(
                                Arrays.asList(new Result.ComplainSystemError(ex.hashCode(), ex.getLocalizedMessage())))),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        List<Result.ComplainSystemError> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(new Result.ComplainSystemError(error.hashCode(), error.getDefaultMessage(), error.getField()));
        }
        return new ResponseEntity<>(new Result<>(400, "Missing! Object body missing", errors), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        return new ResponseEntity<>(
                new Result<>(400, "Error! unable to parse the given data",
                        new ArrayList<>(
                                Arrays.asList(new Result.ComplainSystemError(ex.hashCode(), ex.getLocalizedMessage())))),
                HttpStatus.BAD_REQUEST);
    }
}

