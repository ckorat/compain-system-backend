package com.complainsystem.complain.controller;

import com.complainsystem.complain.exception.ComplainException;
import com.complainsystem.complain.model.Complain;
import com.complainsystem.complain.model.ComplainAllDetails;
import com.complainsystem.complain.model.ComplainResponse;
import com.complainsystem.complain.service.ComplainService;
import com.complainsystem.response.Result;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("/complaints")
public class ComplainController {

    @Autowired
    ComplainService complainService;

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Complain Added Successfully", response = ComplainResponse.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = ComplainException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ComplainException.class)
    })
    public ResponseEntity<Result<ComplainResponse>> addComplain(@RequestBody(required = false) Complain complain) throws Exception {
        Result<ComplainResponse> result = new Result<>(201, complainService.addComplain(complain));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "To Get All Complain Data", response = ComplainAllDetails.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = ComplainException.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = ComplainException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ComplainException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ComplainException.class)
    })
    public ResponseEntity<Result<List<ComplainAllDetails>>> getAllComplain() throws Exception {
        Result<List<ComplainAllDetails>> result = new Result<>(200, complainService.getAllComplain());
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/{role}/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "To Get All Complain Data User Wise", response = ComplainAllDetails.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = ComplainException.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = ComplainException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ComplainException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ComplainException.class)
    })
    public ResponseEntity<Result<List<ComplainAllDetails>>> getAllComplainByUsername(@PathVariable("role") String role, @PathVariable("username") String username) throws Exception {
        Result<List<ComplainAllDetails>> result = new Result<>(200, complainService.getAllComplainByRole(role, username));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/{referenceNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "To Get All Complain Data Reference Number Wise", response = ComplainAllDetails.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = ComplainException.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = ComplainException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ComplainException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ComplainException.class)
    })
    public ResponseEntity<Result<List<ComplainAllDetails>>> getAllComplainByRefNumber(@PathVariable("referenceNumber") @Valid @Pattern(regexp = "[0-9]*") String refNumber) throws Exception {
        Result<List<ComplainAllDetails>> result = new Result<>(200, complainService.getAllComplainByRefNumber(refNumber));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping(value = "/status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "To Get All Complain Data Status Wise", response = ComplainAllDetails.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = ComplainException.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = ComplainException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ComplainException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ComplainException.class)
    })
    public ResponseEntity<Result<List<ComplainAllDetails>>> getAllComplainByStatus(@PathVariable("status") String status) throws Exception {
        Result<List<ComplainAllDetails>> result = new Result<>(200, complainService.getAllComplainByStatus(status));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
}
