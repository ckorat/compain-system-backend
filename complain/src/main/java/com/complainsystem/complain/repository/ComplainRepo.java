package com.complainsystem.complain.repository;

import com.complainsystem.complain.model.Complain;
import com.complainsystem.complain.model.ComplainAllDetails;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplainRepo {
    boolean addComplain(Complain complain) throws Exception;

    List<ComplainAllDetails> getAllComplain() throws Exception;

    String getFreeEmployeeUser() throws Exception;

    List<ComplainAllDetails> getAllComplainByRole(String role, String username) throws Exception;

    List<ComplainAllDetails> getAllComplainByRefNumber(String refNumber) throws Exception;

    List<ComplainAllDetails> getAllComplainByStatus(String status) throws Exception;
}
