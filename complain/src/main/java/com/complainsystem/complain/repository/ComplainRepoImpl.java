package com.complainsystem.complain.repository;

import com.complainsystem.complain.model.Complain;
import com.complainsystem.complain.model.ComplainAllDetails;
import com.complainsystem.complain.model.mapper.ComplainRowMapper;
import com.complainsystem.complain.utils.ServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Repository(value = "complainRepo")
public class ComplainRepoImpl implements ComplainRepo {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ServiceProperties serviceProperties;

    @Override
    public boolean addComplain(Complain complain) throws Exception {
        int i = jdbcTemplate.update(serviceProperties.getDbQueries().getInsertComplain(), complain.getCategoryId(),
                complain.getComplainee(), complain.getEmployeeId(), complain.getSubject(), complain.getShortDesc(),
                complain.getDescription(), complain.getAreaId(), complain.getComplainDate(), complain.getStatus(),
                complain.getRefNumber());
        if (i > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<ComplainAllDetails> getAllComplain() throws Exception {
        return jdbcTemplate.query(serviceProperties.getDbQueries().getGetComplain(), new ComplainRowMapper());
    }

    @Override
    public String getFreeEmployeeUser() throws Exception {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(serviceProperties.getDbQueries().getGetFreeEmployeeUsers());
        if (!list.isEmpty()) {
            return list.get(0).get("username").toString();
        } else {
            List<Map<String, Object>> list1 = jdbcTemplate.queryForList(serviceProperties.getDbQueries().getGetFreeEmployeeComplain());
            return list1.get(0).get("username").toString();
        }
    }

    @Override
    public List<ComplainAllDetails> getAllComplainByRole(String role, String username) throws Exception {
        if (role.equalsIgnoreCase("user")) {
            return jdbcTemplate.query(serviceProperties.getDbQueries().getGetComplainByUser(), new PreparedStatementSetter() {
                public void setValues(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.setString(1, username);
                }
            }, new ComplainRowMapper());
        } else {
            return jdbcTemplate.query(serviceProperties.getDbQueries().getGetComplainByEmployee(), new PreparedStatementSetter() {
                public void setValues(PreparedStatement preparedStatement) throws SQLException {
                    preparedStatement.setString(1, username);
                }
            }, new ComplainRowMapper());
        }

    }

    @Override
    public List<ComplainAllDetails> getAllComplainByRefNumber(String refNumber) throws Exception {
        return jdbcTemplate.query(serviceProperties.getDbQueries().getGetComplainByRefNumber(), new PreparedStatementSetter() {
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, refNumber);
            }
        }, new ComplainRowMapper());
    }

    @Override
    public List<ComplainAllDetails> getAllComplainByStatus(String status) throws Exception {
        return jdbcTemplate.query(serviceProperties.getDbQueries().getGetComplainByStatus(), new PreparedStatementSetter() {
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, status);
            }
        }, new ComplainRowMapper());
    }
}
