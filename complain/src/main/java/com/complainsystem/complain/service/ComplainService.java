package com.complainsystem.complain.service;

import com.complainsystem.complain.model.Complain;
import com.complainsystem.complain.model.ComplainAllDetails;
import com.complainsystem.complain.model.ComplainResponse;
import com.complainsystem.complain.repository.ComplainRepo;
import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ComplainService {
    @Autowired
    @Qualifier("complainRepo")
    ComplainRepo complainRepo;

    public ComplainResponse addComplain(Complain complain) throws Exception {
        complain.setRefNumber(generateRefNumber());
        complain.setEmployeeId(getFreeEmployee());
        complain.setStatus("In progress");
        if (complainRepo.addComplain(complain)) {
            return ComplainResponse.builder().message("Your Compalin Is Added Succesfully. This Is Your Reference Number" + complain.getRefNumber()).build();
        }
        throw new ResultException(new Result<>(500, "We Are Facing Some Issue With Your Request. Please Try Again Later."));
    }

    public List<ComplainAllDetails> getAllComplain() throws Exception {
        List<ComplainAllDetails> list = complainRepo.getAllComplain();
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "We Are Not Able To Process Your Request Right Now."));
    }

    public String generateRefNumber() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public String getFreeEmployee() throws Exception {
        return complainRepo.getFreeEmployeeUser();
    }

    public List<ComplainAllDetails> getAllComplainByRole(String role, String username) throws Exception {
        List<ComplainAllDetails> list = complainRepo.getAllComplainByRole(role, username);
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "We Are Not Able To Process Your Request Right Now."));
    }

    public List<ComplainAllDetails> getAllComplainByRefNumber(String refNumber) throws Exception {
        List<ComplainAllDetails> list = complainRepo.getAllComplainByRefNumber(refNumber);
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "We Are Not Able To Process Your Request Right Now."));
    }

    public List<ComplainAllDetails> getAllComplainByStatus(String status) throws Exception {
        List<ComplainAllDetails> list = complainRepo.getAllComplainByStatus(status);
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "We Are Not Able To Process Your Request Right Now."));
    }
}
