package com.complainsystem.complain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ComplainAllDetails extends Complain {
    private String areaName;
    private String pinCode;
    private String categoryName;
}
