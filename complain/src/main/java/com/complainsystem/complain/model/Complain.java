package com.complainsystem.complain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor

public class Complain {

    private int complainId;
    private int categoryId;
    private String complainee;
    private String employeeId;
    private String subject;
    private String shortDesc;
    private String description;
    private int areaId;
    private Date complainDate;
    private Date complainCompletedDate;
    private String status;
    private String refNumber;
}
