package com.complainsystem.complain.model.mapper;

import com.complainsystem.complain.model.ComplainAllDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ComplainRowMapper implements RowMapper<ComplainAllDetails> {

    @Override
    public ComplainAllDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        ComplainAllDetails complain = new ComplainAllDetails();
        complain.setComplainId(rs.getInt("compid"));
        complain.setCategoryId(rs.getInt("catid"));
        complain.setComplainee(rs.getString("complainee"));
        complain.setEmployeeId(rs.getString("assignedto"));
        complain.setSubject(rs.getString("subjact"));
        complain.setShortDesc(rs.getString("shortdesc"));
        complain.setDescription(rs.getString("description"));
        complain.setAreaId(rs.getInt("areaid"));
        complain.setComplainDate(rs.getDate("compdate"));
        complain.setComplainCompletedDate(rs.getDate("completedate"));
        complain.setStatus(rs.getString("status"));
        complain.setRefNumber(rs.getString("refnumber"));
        complain.setAreaName(rs.getString("name"));
        complain.setPinCode(rs.getString("pincode"));
        complain.setCategoryName(rs.getString("catname"));
        return complain;
    }

}
