package com.complainsystem.solution.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Solution {
    private int solutionId;
    private long complaintReferenceNumber;
    private String solution;

}
