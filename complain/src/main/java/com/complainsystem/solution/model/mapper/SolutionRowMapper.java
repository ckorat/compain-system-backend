package com.complainsystem.solution.model.mapper;

import com.complainsystem.solution.model.Solution;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class SolutionRowMapper implements RowMapper<Solution> {

    @Override
    public Solution mapRow(ResultSet rs, int rowNum) throws SQLException {
        Solution solution = new Solution();
        solution.setComplaintReferenceNumber(rs.getLong("refnumber"));
        solution.setSolutionId(rs.getInt("solutionid"));
        solution.setSolution(rs.getString("solution"));
        return solution;
    }

}