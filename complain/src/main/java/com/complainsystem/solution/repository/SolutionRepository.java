package com.complainsystem.solution.repository;

import com.complainsystem.solution.model.Solution;

import java.util.List;

public interface SolutionRepository {

    List<Solution> findAllSolutionByComplainId(long compId);

    int addSolutionByComplainId(Solution solution, String status) throws Exception;
}
