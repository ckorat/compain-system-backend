package com.complainsystem.solution.repository;

import com.complainsystem.solution.model.Solution;
import com.complainsystem.solution.model.mapper.SolutionRowMapper;
import com.complainsystem.solution.service.SolutionQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class SolutionRepositoryImpl implements SolutionRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    SolutionQueryService solutionQueryService;

    @Override
    public List<Solution> findAllSolutionByComplainId(long compId) {
        return jdbcTemplate.query(solutionQueryService.dbQueries.getSolutionById, new SolutionRowMapper(), compId);
    }

    @Override
    public int addSolutionByComplainId(Solution solution, String status) {
        int updateResult = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        int result = jdbcTemplate.update(solutionQueryService.dbQueries.addSolution, solution.getComplaintReferenceNumber(), solution.getSolution());
        if (result > 0) {
            updateResult = jdbcTemplate.update(solutionQueryService.dbQueries.updateComplainStatus, status, formatter.format(currentDate), solution.getComplaintReferenceNumber());
        }
        return updateResult;
    }
}
