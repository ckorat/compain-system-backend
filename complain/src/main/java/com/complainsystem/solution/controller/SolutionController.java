package com.complainsystem.solution.controller;

import com.complainsystem.response.Result;
import com.complainsystem.solution.exception.SolutionException;
import com.complainsystem.solution.model.Solution;
import com.complainsystem.solution.service.SolutionService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping("{complaintReferenceNumber}/solutions")
public class SolutionController {

    @Autowired
    SolutionService solutionService;

    @GetMapping(value = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get  Solution on Reference number ", response = Solution.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = SolutionException.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = SolutionException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = SolutionException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = SolutionException.class)
    })
    public ResponseEntity<Result<List<Solution>>> getSolutionByComplainId(@PathVariable("complaintReferenceNumber") @Valid @Pattern(regexp = "[0-9]*") long complaintReferenceNumber) {
        Result<List<Solution>> result = new Result<>(200, solutionService.getSolutionByComplainId(complaintReferenceNumber));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));

    }

    @PostMapping(value = "/{status}", produces = "application/json", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Solution Added Successfully", response = Solution.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = SolutionException.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = SolutionException.class)
    })
    public ResponseEntity<Result<Solution>> addSolutionByComplainId(@RequestBody(required = true) @Valid Solution solution, @PathVariable("complaintReferenceNumber") @Valid @Pattern(regexp = "[0-9]*") long complaintReferenceNumber, @PathVariable("status") String status) throws Exception {
        Result<Solution> result = solutionService.addSolutionByComplainId(solution, complaintReferenceNumber, status);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));

    }


}
