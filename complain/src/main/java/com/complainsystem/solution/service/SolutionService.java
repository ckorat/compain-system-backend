package com.complainsystem.solution.service;

import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import com.complainsystem.solution.model.Solution;
import com.complainsystem.solution.repository.SolutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolutionService {

    @Autowired
    SolutionRepository solutionRepository;

    public List<Solution> getSolutionByComplainId(long refNo) {
        List<Solution> list = solutionRepository.findAllSolutionByComplainId(refNo);
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "No solution found for this Id."));
    }

    public Result<Solution> addSolutionByComplainId(Solution solution, long refNo, String status) throws Exception {
        solution.setComplaintReferenceNumber(refNo);
        int id = solutionRepository.addSolutionByComplainId(solution, status);
        if (id > 0) {
            return new Result<>(201, solution);
        }
        throw new ResultException(new Result<>(400, "Error!, please try again!"));
    }


}
