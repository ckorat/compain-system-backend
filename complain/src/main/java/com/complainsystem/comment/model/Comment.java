package com.complainsystem.comment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    private int commentId;
    private long complaintReferenceNumber;
    private String commenter;
    private String comment;
    private Date commentDate;

}
