package com.complainsystem.comment.model.mapper;

import com.complainsystem.comment.model.Comment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentRowMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Comment comment = new Comment();
        comment.setCommentId(rs.getInt("commentid"));
        comment.setComplaintReferenceNumber(rs.getLong("refnumber"));
        comment.setCommenter(rs.getString("commenter"));
        comment.setComment(rs.getString("comment"));
        comment.setCommentDate(rs.getDate("commentdate"));
        return comment;
    }

}
