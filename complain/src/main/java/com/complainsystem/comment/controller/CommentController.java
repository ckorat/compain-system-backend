package com.complainsystem.comment.controller;

import com.complainsystem.comment.model.Comment;
import com.complainsystem.comment.repository.CommentRepository;
import com.complainsystem.comment.service.CommentService;
import com.complainsystem.response.Result;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.List;

@RestController
@RequestMapping(value = "{complaintReferenceNumber}/comments", produces = "application/json")
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping(value = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get All comments on Reference number ", response = Comment.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = CommentRepository.class),
            @ApiResponse(code = 401, message = "UnAuthorized", response = CommentRepository.class),
            @ApiResponse(code = 403, message = "Forbidden", response = CommentRepository.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommentRepository.class)
    })
    public ResponseEntity<Result<List<Comment>>> getAllCommentsByComplainId(@PathVariable("complaintReferenceNumber") @Valid @Pattern(regexp = "[0-9]*") long complaintReferenceNumber) {
        Result<List<Comment>> result = new Result<>(200, commentService.findAllCommentsByComplainId(complaintReferenceNumber));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Comment Added Successfully", response = Comment.class),
            @ApiResponse(code = 404, message = "Service Not Found", response = CommentRepository.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = CommentRepository.class)
    })
    public ResponseEntity<Result<Comment>> addCommentByComplainId(@RequestBody(required = true) @Valid Comment comment, @PathVariable("complaintReferenceNumber") @Valid @Pattern(regexp = "[0-9]*") long complaintReferenceNumber) throws Exception {
        Result<Comment> result = commentService.addCommentByComplainId(comment, complaintReferenceNumber);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

}
