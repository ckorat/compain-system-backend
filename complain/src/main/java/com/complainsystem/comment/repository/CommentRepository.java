package com.complainsystem.comment.repository;

import com.complainsystem.comment.model.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> findAllCommentsByComplainId(long compId);

    int addCommentByComplainId(Comment comment) throws Exception;
}
