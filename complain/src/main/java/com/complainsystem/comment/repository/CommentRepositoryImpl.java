package com.complainsystem.comment.repository;

import com.complainsystem.comment.model.Comment;
import com.complainsystem.comment.model.mapper.CommentRowMapper;
import com.complainsystem.comment.service.CommentQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {


    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CommentQueryService commentQueryService;

    @Override
    public List<Comment> findAllCommentsByComplainId(long compId) {
        return jdbcTemplate.query(commentQueryService.dbQueries.getCommentById, new CommentRowMapper(), compId);
    }

    @Override
    public int addCommentByComplainId(Comment comment) throws IllegalArgumentException {

        return jdbcTemplate.update(commentQueryService.dbQueries.insertComment, comment.getComment(), comment.getComplaintReferenceNumber(), comment.getCommentDate(), comment.getCommenter());
    }

}
