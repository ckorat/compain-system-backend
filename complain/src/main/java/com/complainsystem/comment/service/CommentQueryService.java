package com.complainsystem.comment.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "db")
@Component
public class CommentQueryService {

    @NotNull
    public DbQuery dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQuery {
        public String getCommentById;
        public String insertComment;

    }
}
