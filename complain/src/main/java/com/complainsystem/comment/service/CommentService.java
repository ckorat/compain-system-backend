package com.complainsystem.comment.service;

import com.complainsystem.comment.model.Comment;
import com.complainsystem.comment.repository.CommentRepository;
import com.complainsystem.exception.ResultException;
import com.complainsystem.response.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    public List<Comment> findAllCommentsByComplainId(long compId) {
        List<Comment> list = commentRepository.findAllCommentsByComplainId(compId);
        if (!list.isEmpty())
            return list;
        throw new ResultException(new Result<>(404, "No comments found for this Id."));

    }

    public Result<Comment> addCommentByComplainId(Comment comment, long complainId) throws Exception {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date(System.currentTimeMillis());
        comment.setComplaintReferenceNumber(complainId);
        comment.setCommentDate(currentDate);
        int id = commentRepository.addCommentByComplainId(comment);
        if (id > 0) {
            return new Result<>(201, comment);
        }
        throw new ResultException(new Result<>(404, "Error!, please try again!"));
    }
}
